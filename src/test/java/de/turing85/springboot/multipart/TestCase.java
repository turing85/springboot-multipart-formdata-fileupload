package de.turing85.springboot.multipart;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
class TestCase {

  @Autowired
  private MockMvc mockMvc;

  @Test
  void shouldReturnOk() throws Exception {
    // GIVEN
    final byte[] content = Files.readAllBytes(Path.of(".", "src/test/resources/PenPen.png"));
    final String name = "name";

    // WHEN
    // @formatter:off
    mockMvc.perform(MockMvcRequestBuilders
        .multipart(Resource.ROOT)
            .file("file", content)
            .param("name", name))

    // THEN
        .andExpect(status().isOk());
    // @formatter:on
  }
}
