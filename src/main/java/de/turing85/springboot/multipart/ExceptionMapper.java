package de.turing85.springboot.multipart;

import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionMapper {

  /**
   * Maps BindExceptions to 400 BAD REQUEST status.
   *
   * @param e the exception
   * @return a 400 response.
   */
  @ExceptionHandler(BindException.class)
  public ResponseEntity<String> handleBindException(final BindException e) {
    final String message = collectAllValidationErrorsAsOneMessage(e.getBindingResult());
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(message);
  }

  private String collectAllValidationErrorsAsOneMessage(final BindingResult bindingResult) {
    return bindingResult.getFieldErrors().stream()
        .map(error -> error.getField() + ": " + error.getDefaultMessage())
        .collect(Collectors.joining(". "))
        .concat(".");
  }
}
