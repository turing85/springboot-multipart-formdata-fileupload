package de.turing85.springboot.multipart;

import java.lang.reflect.Field;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.MethodParameter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@SpringBootApplication
@EnableWebSecurity
public class Application extends WebSecurityConfigurerAdapter {

  public static void main(final String... args) {
    SpringApplication.run(Application.class, args);
  }

  @Override
  public void configure(final HttpSecurity http) throws Exception {
    // @formatter:off
    http
        .httpBasic().disable()
        .csrf().disable()
        .authorizeRequests().antMatchers("/**").permitAll()
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    // @formatter:on
  }

  /**
   * Bean enabling logger injection.
   *
   * @param injectionPoint injectionPoint.
   * @return the logger instance to inject.
   */
  @Bean
  @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public Logger logger(final InjectionPoint injectionPoint) {
    // @formatter:off
    return LoggerFactory
        .getLogger(Optional.ofNullable(injectionPoint.getMethodParameter())
            .<Class<?>>map(MethodParameter::getContainingClass)
            .orElseGet(() -> Optional.ofNullable(injectionPoint.getField())
                .map(Field::getDeclaringClass)
                .orElseThrow(IllegalArgumentException::new)
            )
        );
    // @formatter:on
  }
}
