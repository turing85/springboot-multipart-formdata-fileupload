package de.turing85.springboot.multipart;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.web.multipart.MultipartFile;

@Value
@AllArgsConstructor
public class Request {

  @NotNull
  String name;

  @NotNull
  MultipartFile file;
}
